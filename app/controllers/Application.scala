package controllers

import play.api._
import play.api.mvc._

object Application extends Controller {

	val typoDomains = Map (
		"git.com" -> "av.com",
		"soogle.com" -> "av.com",
		"soogle.org" -> "av.com"
		)


  def index = Action {
    Ok(views.html.index())
  }

  def letMeSpell(typoDomain: String) = Action {
  	 Logger.info(s"Domain typo $typoDomain")
  	 val safeDomain = typoDomain
  	 val targetDomain = typoDomains.get(safeDomain)
    Ok(views.html.typo(safeDomain,targetDomain))
  }

  def about = Action {
    Ok(views.html.about())
  }

  def showContact = Action {
    Ok(views.html.contact())
  }

  def showSupport = Action {
    Ok(views.html.support())
  }

  def showRegister = TODO

  def showLogin = TODO


}